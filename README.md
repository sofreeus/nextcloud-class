# Build your own Nextcloud cloud

![Banner](lessons/images/banner.png)

Nextcloud is like your own personal Dropbox, OneDrive, or whatever, that you control. You decide where the data goes and who gets access to it.

Nextcloud is free and open-source, distributed under the AGPL with clients for many platforms.

## Who I Am

[Emma Shoup](https://shoup.io/)

## Class Objectives

1. Why to use Nextcloud
2. How to share files with others using Nextcloud federation
3. How to use Nextcloud apps like Talk and Draw.io
4. Backing up and restoring Nextcloud

## Requirements

1. A web browser
2. An SSH client
3. Some basic Linux skills would be a plus

## Lessons

1. [Accessing your Nextcloud instance](lessons/01_accessing_nextcloud.md)
2. [Installing Nextcloud clients](lessons/02_install_nextcloud_client.md)
3. [Using Nextcloud federation](lessons/03_sharing_with_nextcloud.md)
4. [Nextcloud apps](lessons/04_nextcloud_apps.md)
5. [Backing up Nextcloud](lessons/05_nextcloud_backup_and_restore.md)

## Infrastructure

All infrastructure for this class runs on Linode. The instances were built using Terraform. You can find the Terraform code in the [`terraform`](terraform/) folder.

## License

![CC-BY](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)
The contents of this class, except the Nextcloud logo and Software Freedom School logo, are licensed under a [Create Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

Information on Nextcloud: <https://nextcloud.com/>

Information on the Software Freedom School: <http://sofree.us/>
