# Apps and Nextcloud

Apps allow you to extend Nextcloud.

When you set up Nextcloud, you got the option to install a recommended set of apps.

Let's explore some of them.

## Talk

Talk allows you to have conversations and do video conferencing things. Create a conversation, set the permissions to allow guests and send a link to your neighbor.

## Install Apps

Install the apps via the admin panel.

Why don't you install one of my favorite Nextcloud apps, 'draw.io'?
