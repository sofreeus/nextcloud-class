# Accessing your Nextcloud Instance

A list of instances and passwords for the SSH account can be found in Mattermost.

Follow the instructor's instructions for instantiating installations.

## Finish Setting up Nextcloud

Nextcloud is already installed using the [Nextcloud snap](https://github.com/nextcloud-snap/nextcloud-snap). Snaps are similar to containers, and the Nextcloud snap contains everything that's needed to run Nextcloud.

Just a few steps are needed to finish.

1. Point your web browser to your instance.
2. Create an admin account. To make sharing easier, use your name for the username.
3. Install the recommended set of apps.

Note: Steps 2 and 3 can take a few minutes to complete.

## Access your instance via SSH

Your instance has a student account preconfigured with a password for SSH.

1. SSH to your instance using the student account.
2. Change the student password to something no one else knows.
3. Test your sudo access.
4. Authorize your SSH key if you want to.

## Next

Now that your instance is configured, let's [configure a nextcloud client](02_install_nextcloud_client.md)
