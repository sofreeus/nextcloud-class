# Sharing is caring

Let's be nice and share some stuff.

You can share with:

* Other users on your Nextcloud server
* Users on other Nextcloud servers
* The public Internet

## Users and groups

To share with other users on your Nextcloud server, you need to create other users!

Do the following:

1. Create user `Alice`
2. Make `Alice` a member of the group `Accounting`, also make her an admin of `Accounting`
3. Log in as `Alice`, and have `Alice` create accounts for her colleagues, `Bob` and `Charlie`.

## Share with users on your Nextcloud server

1. Create a file with your account and share it with `Alice`. Login with `Alice`'s account and verify the file was shared.
2. As `Alice`, create a folder named 'Accounting Stuff' and share with all of the `accounting` group.

## Share with users on other Nextcloud servers

1. Look at the person next to you, get their account's Nextcloud URL.
2. Share a file or folder with them, and have them share with you!

## Share with the public Internet

1. Share a file with the public.
2. Have someone else open the URL, or open it on your phone

## Next

Now that you've shared some stuff, let's learn about [additional apps.](04_nextcloud_apps.md)
