# Installing Nextcloud clients

## What the clients do

### Desktop clients

Desktop clients are the bread and butter of Nextcloud, and are what most people think of when they hear "Dropbox".

The desktop client allows you to sync a folder (Commonly `~/Nextcloud` or `C:\Users\<username>\Nextcloud`) with a central server. When syncing is enabled and working, dropping a file in the Nextcloud folder will upload it to the server and other clients will have the ability to sync it.

### Mobile clients

Clients for your smartphone are also available and come with some neat features. For example, you can configure it to automatically upload any pictures taken from your phone to your Nextcloud server. Files can also be downloaded and synchronized individually.

## Installing clients

For the class, it is entirely optional, but recommended to install an app and play with syncing. The rest of the class can be done without the desktop app if you choose to do so.

Visit the download page and download the appropriate installer: https://nextcloud.com/install/#install-clients

### Configure clients

1. Launch your freshly installed client
2. For server address, enter your server's name
3. Enter the credentials for the account you created.
4. Choose what to sync (Everything) and be done!

### Play with syncing

Drop a file in your local Nextcloud folder. Wait a short moment and see how it appears in the web browser client.

Upload a file in the web client. Wait a short moment and see how it appears in the desktop folder.

## Next

Now that you're syncing, let's [share some files.](03_sharing_with_nextcloud.md)
