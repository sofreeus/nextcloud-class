# Backup and Restore

The 3-2-1 backup strategy:

* You should have 3 copies of your data
* 2 copies of your data can be in the same physical location
* 1 copy of your data must be off site

## A backup strategy for Nextcloud

1. [Put nextcloud in maintenance mode](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/index.html) - this is important!
2. Dump the database and copy to another location.
3. Copy the data directory to another location.
4. Regularly test your backups!

## Backing up a snap installation

The nextcloud snap contains a utility to automatically perform the first 3 steps from above. [You can run `nextcloud.export`](https://github.com/nextcloud-snap/nextcloud-snap/wiki/How-to-backup-your-instance).

`nextcloud.export` creates a backup copy of your data. This can count as your second copy, however, you should probably copy this data to at least another system in the same location. Finally, copy that data somewhere offsite.

## Restoring a snap installation

You can use the `nextcloud.import` tool by running it and pointing to an extracted copy of your data. The data also needs to be owned by root. [Reading the documentation is helpful here](https://github.com/nextcloud-snap/nextcloud-snap/wiki/How-to-backup-your-instance#import-everything).

## Backup your nextcloud instance

1. Run the `nextcloud.export` utility (you'll need to use `sudo`).
2. Create an archive of the backup (`sudo tar zcf ~/nextcloud-backup.tar.gz $BACKUP_PATH`).
3. In a real life situation, you would copy this archive to an offsite location.

## Restore your nextcloud instance

1. Destroy your nextcloud instance! `sudo snap remove nextcloud`
2. Visit your nextcloud instance's website. Have mild panic attack.
3. Remember you have a backup copy of your data! Panic attack over, but you're still sweating.
4. Re-install Nextcloud, and reconfigure SSL
5. Extract and import.
6. Feel a sense of relief.

Importing with the utility is a little tricky. Here's a cheat sheet.

```shell
sudo snap install nextcloud
sudo nextcloud.enable-https lets-encrypt
sudo mkdir /var/snap/nextcloud/common/restore
cd /var/snap/nextcloud/common/restore
sudo tar xf ~/nextcloud-backup.tar.gz
sudo nextcloud.import var/snap/nextcloud/common/backups/<timestamp>
```
