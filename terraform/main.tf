data "template_file" "init_scripts" {
  for_each = {
    for instance in var.instances : instance.label => instance
  }

  template = file("scripts/init.sh")

  vars = {
    hostname          = "${each.value.label}.${var.domain_name}"
    student_password  = "${each.value.student_password}"
    letsencrypt_email = var.letsencrypt_email
  }
}

resource "linode_stackscript" "instances" {
  for_each = {
    for instance in var.instances : instance.label => instance
  }

  label       = each.value.label
  description = "Build a nextcloud box for students."
  images      = [var.linode_image]
  script      = data.template_file.init_scripts[each.value.label].rendered
}

resource "linode_instance" "instances" {
  for_each = {
    for instance in var.instances : instance.label => instance
  }

  label            = each.value.label
  image            = var.linode_image
  region           = var.linode_region
  type             = var.linode_instance_type
  authorized_users = [var.linode_admin_username]
  stackscript_id   = linode_stackscript.instances[each.value.label].id
}

data "linode_domain" "domain" {
  domain = var.domain_name
}

output "ipv4" {
  value = { for k, v in linode_instance.instances : k => tolist(v.ipv4)[0] }
}

resource "linode_domain_record" "records" {
  for_each = {
    for k, v in linode_instance.instances : k => tolist(v.ipv4)[0]
  }

  domain_id   = data.linode_domain.domain.id
  name        = each.key
  record_type = "A"
  target      = each.value
  ttl_sec     = 120
}
