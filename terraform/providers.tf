terraform {
  required_version = "~> 1.1.7"

  required_providers {
    linode = {
      source  = "linode/linode"
      version = "~> 1.27.0"
    }

    template = {
      source  = "hashicorp/template"
      version = "~> 2.2.0"
    }
  }
}

provider "linode" {
  token = var.linode_token
}

