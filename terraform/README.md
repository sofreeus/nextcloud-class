# Terraform Code

This directory contains a Terraform module for building the infrastructure used in this class.

## Requirements

* Terraform 1.1.x
* A Linode Account
* A Linode API Key
* A domain configured in Linode

## Linode Domain

You will need to configure a domain in Linode, and point set the nameservers to Linode's.

To help with DNS resolution while the instances start, configure the Default TTL, Refresh Rate, and Retry Rate all to 30 seconds.

## Usage

Copy `example.tfvars` to `my.auto.tfvars`. The file `my.auto.tfvars` is automatically excluded in `.gitignore`.

Edit `my.auto.tfvars` and set the values to your liking. Other variables can be modified. See `variables.tf`.

Then, execute the module:

```shell
terraform init
terraform apply
```

After the apply is complete, it will take about 5 minutes for each instance to become available. After 5 minutes, you should be able to access the instances as root using your configured Linode SSH key, or as the student user using the student password. Also, Nextcloud should be available at `https://${hostname}`.

To destroy:

```shell
terraform destroy
```

## Local Terraform State

This module is configured with local terraform state. Do not delete the `terraform.tfstate` file until you have destroyed your infrastrcuture.
