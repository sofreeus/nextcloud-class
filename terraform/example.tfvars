linode_token          = "your linode token here"
domain_name           = "your domain name"
linode_admin_username = "your linode username"
letsencrypt_email     = "your email"

instances = [
  {
    label            = "alpaca"
    student_password = "pass1234"
  },
  {
    label            = "bison"
    student_password = "pass2345"
  },
]
