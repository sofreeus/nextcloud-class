variable "linode_token" {
  description = "Your Linode API token."
  type        = string
}

variable "domain_name" {
  description = "Name of the domain setup with DNS at Linode"
  type        = string
}

variable "instances" {
  description = "A list of instances to create. Passwords are randomly generated and provided in output."
  type = list(object({
    label            = string
    student_password = string
  }))
}

variable "linode_admin_username" {
  description = "Username of linode user to give ssh access to (retrieves their SSH keys)"
  type        = string
}

variable "letsencrypt_email" {
  description = "Email to send letsencrypt notifications to"
  type        = string
}

variable "linode_image" {
  description = "Image used to build instances"
  type        = string
  default     = "linode/ubuntu20.04"
}

variable "linode_region" {
  description = "Region to build resources in"
  type        = string
  default     = "us-west"
}

variable "linode_instance_type" {
  description = "Instance type to build"
  type        = string
  default     = "g6-dedicated-2"
}
