# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/template" {
  version     = "2.2.0"
  constraints = "~> 2.2.0"
  hashes = [
    "h1:94qn780bi1qjrbC3uQtjJh3Wkfwd5+tTtJHOb7KTg9w=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version     = "1.27.0"
  constraints = "~> 1.27.0"
  hashes = [
    "h1:HFkSqvQhiYPI6CZPpDDD/IqfD9Y+eCm0rBQi+XPPNHE=",
    "zh:054990485be1a1d193cce7e3093550bade07df4b124a1b6e4c54e1050360b9b8",
    "zh:219dff372673247f0ad5aa528700f9b54a867409d874e0e16cd99b9835c048e2",
    "zh:37732286a8d6029096bbd1ef70d4e9bcdf8a034aa32fc0cfcc2642c479200f0a",
    "zh:3e7b55d9dc605bfd37d81c9307ba148dd715de2136694e438aa834a844640ca6",
    "zh:50b5d2d3223eb55c2ff26ea5bf57d7a1cb1331b63459af03434e99243d32c976",
    "zh:5109cc394206e9cddd68d810ff247854f920bdc1123dcfe2d8f4f1ccee7b1efe",
    "zh:6f17fa2b74162886539f1e1f390be3a94f2008a73a54d9af726aef6452e1e5f0",
    "zh:78f183dd6943885d00bf546f28f0aef548a57ce378074dd5b979c92abeb602d1",
    "zh:a28b920c707ab1cf3d2df76900c527f972a674dff77650d764a2570aa9d6bcf5",
    "zh:aa65cabd34f4a6443dd462b0a0c838944b869cc30e4396d4c0a6cab9e85d00bd",
    "zh:ad85b9e434472d82999cf3482b5c462f21fd9714b45cdd9524a2bc2d84f50eb4",
    "zh:b3670d43cbcd3de14c604c1462c194967debc3d868ddd7b5c958e7b379935b7f",
    "zh:c371dfba71f66a438c35f1ccb23b5af8d9ef376084e0e5bc166b32f48eb1c099",
    "zh:c7b9cdafd7cf58133eac4dd2aa7fe0421e80b8cac6905b0fcdfc72b64b7cabc2",
  ]
}
