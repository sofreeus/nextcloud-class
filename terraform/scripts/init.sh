#!/bin/bash
set -x
exec >/root/stackscript.log 2>&1
echo "Begin at $(date -Is)"

echo 'Setting hostname...'
hostnamectl set-hostname ${hostname}

echo 'Creating student user...'
useradd -m -s /bin/bash -k /etc/skel student
echo 'student:${student_password}' | chpasswd
echo 'student ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/student

echo 'Installing nextcloud snap...'
snap install nextcloud

echo 'Sleep 120 seconds to help ensure DNS record is available.'
sleep 120
echo 'Checking if DNS record exists...'
while [ -z $(dig +short ${hostname} @1.1.1.1) ]; do
	echo 'DNS record not found. Sleeping 30 more seconds...'
	sleep 30
done

echo 'Enabling lets encrypt...'
nextcloud.enable-https lets-encrypt << EOT
y
${letsencrypt_email}
${hostname}
EOT

echo "End at $(date -Is)"
